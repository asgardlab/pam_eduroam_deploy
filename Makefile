all: eapconfig.tgz lib/pam_eduroam.so
eapconfig.tgz: FORCE
	chmod -R og-w eapconfig
	tar --owner=root --group=root -zcf eapconfig.tgz -C eapconfig .
lib/pam_eduroam.so: src/pam_eduroam/target/release/libpam_eduroam.so
	cp $< $@
	strip --strip-unneeded $@
src/pam_eduroam/target/release/libpam_eduroam.so: FORCE 
	cd src/pam_eduroam; cargo build --release
FORCE:
